# README #

Example code for connecting to mgmt_ctrl over modbus

### Modbus commands ###

mgmt_ctrl expects either function 0x03 Read holding registers or function 0x10 Preset Multiple Registers on the specified addresses to execute its functions.

| Function | Address  | Register Name | Description | Length |
|----------|----------|---------------|-------------|--------|
| 0x03     | 0x0000   | Identifier    |  Returns the MAC-address of the device as a NULL-terminated string with six octets separated by colon and a newline character. I.e. 11:22:33:44:55:66\n the expected response is 9 registers long | 9 |
| 0x10     | 0x0100   | MainMCU download  | Expects a filename, sent by the Preset Multiple Registers function. Download specified target file name to palette. Errors handled by the error response 0x90 + error code 01, file not found. 02 Other error | |
| 0x10     | 0x0200   | AppMCU download  | Expects a filename, sent by the Preset Multiple Registers function. Downlo~~~~ad specified target file name to palette. Errors handled by the error response 0x90 + error code 01, file not found. 02 Other error | |
| 0x10     | 0x0300   | ADuC download  | Expects a filename, sent by the Preset Multiple Registers function. Download specified target file name to palette. Errors handled by the error response 0x90 + error code 01, file not found. 02 Other error | |
| 0x03     | 0x1001-0x1010   | UUT present  | UUT presence detection: 0x00 UUT not present 0x01 UUT is present, the address determines the port (1-16) 0x1001 = 1, 0x1010 = 16 | 1 |
| 0x03     | 0x2001-0x2010   | Program Main MCU | Control programming of port 1-16 for main MCU. 0 - Programming started 1 - Programming started, Powered but presence on port not checked by client. 2 – Programming not started. not powered. 3 - Programming not started, port busy | 1 |
| 0x03     | 0x2021-0x2030   | Program App MCU | Control programming of port 1-16 for App MCU. 0 - Programming started 1 - Programming started, Powered but presence on port not checked by client. 2 – Programming not started. not powered. 3 - Programming not started, port busy| 1 |
| 0x03     | 0x2041-0x2050   | Program ADuC MCU | Control programming of port 1-16 for ADuC. 0 - Programming started 1 - Programming started, Powered but presence on port not checked by client. 2 – Programming not started. not powered. 3 - Programming not started, port busy| 1 |
| 0x10     | 0x2101-0x2110   | Set Power | Expects a single register set to 1 or 0, sent by the Preset Multiple Registers function. Set UUT power of port, the address determines the port 1-16 | |
| 0x03     | 0x6000 | Read Programming status |  Returns the programming status of all DUTS one per register, return codes in table below | 16 |

### Return Data from Read Programming status ###

The function returns status for all DUTs 16 registers or 32 bytes in total, the first byte describes power and presence, the second describes programming status. Used in combination with the instant return codes from the Programming commands i.e. Program Main MCU

| Byte0                       | Byte1    | 
|-----------------------------|----------|
|0x00 not powered or detected | 0x00 not started |
|0x01 powered                 | 0x01 Programming Main MCU started |
|0x02 detected                | 0x02 Programming Main MCU succeeded |
|0x03 powered and detected    | 0x03 Programming Main MCU failed |
|                             | 0x04 Programming App MCU started |
|                             | 0x05 Programming App MCU succeded|
|                             |	0x06 Programming App MCU failed|
|                             |	0x07 Programming Aduc started |
|                             | 0x08 Programming Aduc succeded |
|                             |	0x09 Programming Aduc failed |

### CLI interfaces ###

mgmt_ctrl_mb.py is a small cli interface to connect to the modbus service, this script should be used for everything except read/write RAM and EEPROM from cli where mgmt_ctrl.py still should be used. use mgmt_ctrl_mb.py help for usage info

example usages:  
root@stm32mp1-g33:~# mgmt_ctrl_mb.py presence 1  
Port: 1 present  

root@stm32mp1-g33:~# mgmt_ctrl_mb.py set_power 1 1  
Set Power:1 on port :1  

root@stm32mp1-g33:~# mgmt_ctrl_mb.py status  
[3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]  

root@stm32mp1-g33:~# mgmt_ctrl_mb.py get_id  
Palette identity response  
[b'be', b':e', b'f:', b'01', b':f', b'5:', b'85', b':0', b'd\n']  

root@stm32mp1-g33:~# mgmt_ctrl_mb.py program main 1  
Start main Programming  
Programming main started  
Programming main Port: 1  
Programming main Port: 1  
Programming main Port: 1  
Programming main Port: 1  
Programming main Port: 1  
Programming main Port: 1  
Programming main Port: 1  
Programming main Port: 1  
Programming main Success Port:1  
Many responses since the function is polling the status during programming  

root@stm32mp1-g33:~# mgmt_ctrl_mb.py download main P1300_G3_3_Rev1_01_aux123_Draft_191230pg.hex  
File downloaded: P1300_G3_3_Rev1_01_aux123_Draft_191230pg.hex  

mgmt_ctrl.py used to read and write EEPROM and RAM from cli

root@stm32mp1-g33:~# mgmt_ctrl.py read_eeprom 0x0000 6 1  
fe4606ffffffffffffa101  
root@stm32mp1-g33:~# mgmt_ctrl.py write_eeprom 0x0000 "68 05 02 00 FF FF FF FF 00 00 00 00 FF FF FF 56" 1  
fe430021  
root@stm32mp1-g33:~# mgmt_ctrl.py read_ram 0x0028 4 1  
fe440400000000fa4b  


  