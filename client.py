#!/usr/bin/env python
import concurrent.futures
import time
from timeit import default_timer as timer
import socket

import umodbus
from umodbus import conf
from umodbus.client import tcp

conf.SIGNED_VALUES = True

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('192.168.20.10', 520))
# sock.connect(('localhost', 520))
# sock.connect(('10.0.20.71', 520))



def download(chip, string):
    address = 0x0000
    if chip == 'main':
        address = 0x0100
    elif chip == 'app':
        address = 0x0200
    elif chip == 'aduc':
        address = 0x0300
    error_code = 0
    payload = string.encode('ascii')
    message = tcp.write_multiple_registers(slave_id=254, starting_address=address, values=payload)
    try:
        response = tcp.send_message(message, sock)
    except (umodbus.exceptions.IllegalFunctionError, umodbus.exceptions.IllegalDataAddressError) as e:
        error_code = e.error_code
    if error_code == 1:
        print('File not found: ' + payload.decode('ascii'))
    if error_code == 0:
        print("File downloaded: " + string)


def status():
    message = tcp.read_holding_registers(slave_id=254, starting_address=0x6000, quantity=16)
    response = tcp.send_message(message, sock)
    res_data = []
    for reg in response:
        for b in reg.to_bytes(2, byteorder="big"):
            res_data.append(b)
    print(res_data)


def get_id():
    message = tcp.read_holding_registers(slave_id=254, starting_address=0x0000, quantity=9)
    response = tcp.send_message(message, sock)
    res_data = []
    for b in response:
        res_data.append(b.to_bytes(2, byteorder="big"))
    print("Palette identity response")
    print(res_data)


def set_power(power, port, s):
    address = 0x2100 + port
    value = 0x00
    if power:
        value = 0x01
    message = tcp.write_multiple_registers(slave_id=254, starting_address=address, values=[value])
    response = tcp.send_message(message, s)
    print("Set Power:" + str(power) + " on port :" + str(port))


def presence(port, s):
    address = 0x1000 + port
    message = tcp.read_holding_registers(slave_id=254, starting_address=address, quantity=1)
    response = tcp.send_message(message, s)
    res_data = []
    for reg in response:
        for b in reg.to_bytes(2, byteorder="big"):
            res_data.append(b)

    if res_data[1] == 0x01:
        print("Port: " + str(port) + " present")
    else:
        print("Port: " + str(port) + " not present")
    return res_data


def program(chip, port, s):
    address = 0x0000
    started = 0
    success = 0
    fail = 0
    ret = False
    if chip == 'main':
        address = 0x2000 + port
        started = 0x01
        success = 0x02
        fail = 0x03
    elif chip == 'app':
        address = 0x2020 + port
        started = 0x04
        success = 0x05
        fail = 0x06
    elif chip == 'aduc':
        address = 0x2040 + port
        started = 0x07
        success = 0x08
        fail = 0x09
    else:
        print("Bad target: " + chip)
    print("Start " + chip + " Programming")
    start = timer()
    message = tcp.read_holding_registers(slave_id=254, starting_address=address, quantity=1)
    response = tcp.send_message(message, s)
    res_data = []
    for reg in response:
        for b in reg.to_bytes(2, byteorder="big"):
            res_data.append(b)
    if res_data[1] == 0x00:
        print("Programming " + chip + " started")
    elif res_data[1] == 0x01:
        print("Programming " + chip + " started, Powered but prescance on port not checked by client")
    elif res_data[1] == 0x02:
        print("Programming " + chip + " not started, not powered.")
        exit(-1)
    elif res_data[1] == 0x03:
        print("Programming " + chip + " not started, port busy")
        exit(-1)
    programming = True
    while programming:
        # get status
        message = tcp.read_holding_registers(slave_id=254, starting_address=0x6000, quantity=16)
        response = tcp.send_message(message, s)
        res_data = []
        for reg in response:
            for b in reg.to_bytes(2, byteorder="big"):
                res_data.append(b)
        if res_data[(port - 1) * 2] != 0x00 and res_data[(port - 1) * 2 + 1] == success:
            print("Programming " + chip + " Success Port:" + str(port))
            programming = False
            ret = True
        if res_data[(port - 1) * 2 + 1] == started:
            print("Programming " + chip + " Port: " + str(port))
        if res_data[(port - 1) * 2 + 1] == fail:
            print("Programming " + chip + " Failed Port:" + str(port))
            ret = False
        time.sleep(5.0)
    print(res_data)
    end = timer()
    print(chip + " programming time:", end - start)
    return ret


def run_programming(port):
    sock_int = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock_int.connect(('192.168.20.10', 520))
    # Check presence on port 1
    presence(port, sock_int)
    # set power 1 port 1
    set_power(1, port, sock_int)
    # Program main mcu 1
    program('main', port, sock_int)
    # program app mcu
   # program('app', port, sock_int)
    # Program eeprom and aduc 1
  #  program('aduc', port, sock_int)
    # Set power 0 on port 1
    set_power(0, port, sock_int)
    sock_int.close()

# Modbus TCP/IP.
# Download main mcu data from tftp
print("Download Main MCU hex bad filename")
download('main', 'P1300_G3_3_Rev1_03_aux255_Release_200714pg.hex')
# download('main', 'P1300_G3_3_Rev1_01_aux123_Draft_191230pg.hex1')
print("Download Main MCU hex")
#download('main', 'P1300_G3_3_Rev1_03_aux255_Release_200714pg.hex')
download('main', 'P1300_G3_3_Rev1_01_aux123_Draft_191230pg.hex')
print("Download APP MCU hex")
download('app', 'P1300_G3_3_Rev1_01_aux123_Draft_191230pg.hex')
print("Download ADuC hex")
download('aduc', 'P915_Rev2_03aux03_200601pg.hex')
# get palette identity
get_id()
'''
print("Power on DUT: 1 for read/write RAM/EEPROM")
set_power(1, 1, sock)
print("Open meter socket")
m_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("Send Read RAM request (address 0x28, size 0x04) without CRC")
m_sock.connect(('192.168.20.10', 501))
m_sock.send(bytearray(b'\x00\x00\x00\x00\x00\x05\xFE\x44\x00\x28\x04'))
data = m_sock.recv(15)
print(data[7:])
tmp = data[:4]
print("Send Write RAM request (address 0x28, 0xFF 0xFF 0xFF 0xFF) without CRC")
m_sock.send(bytearray(b'\x00\x00\x00\x00\x00\x09\xfe\x41\x00\x28\x04\x00\x00\x00\x00'))
data = m_sock.recv(15)
print(data)
print("Send Read RAM request (address 0x28, size 0x04) without CRC")
m_sock.send(bytearray(b'\x00\x00\x00\x00\x00\x05\xFE\x44\x00\x28\x04'))
data = m_sock.recv(15)
print(data[7:])
print("Send Write RAM request (address 0x28, write old data back) without CRC")
ba = bytearray(b'\x00\x00\x00\x00\x00\x09\xfe\x41\x00\x28\x04')
ba.append(tmp[0])
ba.append(tmp[1])
ba.append(tmp[2])
ba.append(tmp[3])
m_sock.send(ba)
data = m_sock.recv(15)
set_power(0, 1, sock)
m_sock.close()
sock.close()'''
with concurrent.futures.ThreadPoolExecutor() as executor:
    f1 = executor.submit(run_programming, 1)
   # f2 = executor.submit(run_programming, 2)
    f1.result()
   # f2.result()

