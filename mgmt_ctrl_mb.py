#!/usr/bin/env python
import sys
import time
from timeit import default_timer as timer
import socket

import umodbus
from umodbus import conf
from umodbus.client import tcp

conf.SIGNED_VALUES = True

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('localhost', 520))


# sock.connect(('10.0.20.71', 520))
# sock.connect(('192.168.20.10', 520))


def download(chip, string):
    address = 0x0000
    if chip == 'main':
        address = 0x0100
    elif chip == 'app':
        address = 0x0200
    elif chip == 'aduc':
        address = 0x0300
    error_code = 0
    payload = string.encode('ascii')
    message = tcp.write_multiple_registers(slave_id=254, starting_address=address, values=payload)
    try:
        response = tcp.send_message(message, sock)
    except (umodbus.exceptions.IllegalFunctionError, umodbus.exceptions.IllegalDataAddressError) as e:
        error_code = e.error_code
    if error_code == 1:
        print('File not found: ' + payload.decode('ascii'))
    if error_code == 0:
        print("File downloaded: " + string)


def status():
    message = tcp.read_holding_registers(slave_id=254, starting_address=0x6000, quantity=16)
    response = tcp.send_message(message, sock)
    res_data = []
    for reg in response:
        for b in reg.to_bytes(2, byteorder="big"):
            res_data.append(b)
    print(res_data)
    for x in range(16):
        s = "Dut " + str(x) + ": "
        if res_data[x * 2] == 0:
            s += "Not powered or detected, "
        elif res_data[x * 2] == 1:
            s += "Powered, "
        elif res_data[x * 2] == 2:
            s += "Detected, "
        elif res_data[x * 2] == 3:
            s += "Powered and detected, "
        if res_data[x * 2 + 1] == 0:
            s += "Not started"
        elif res_data[x * 2 + 1] == 1:
            s += "Programming Main MCU started"
        elif res_data[x * 2 + 1] == 2:
            s += "Programming Main MCU succeeded"
        elif res_data[x * 2 + 1] == 3:
            s += "Programming Main MCU failed"
        elif res_data[x * 2 + 1] == 4:
            s += "Programming App MCU started"
        elif res_data[x * 2 + 1] == 5:
            s += "Programming App MCU succeeded"
        elif res_data[x * 2 + 1] == 6:
            s += "Programming App MCU failed"
        elif res_data[x * 2 + 1] == 7:
            s += "Programming Aduc started"
        elif res_data[x * 2 + 1] == 8:
            s += "Programming Aduc succeeded"
        elif res_data[x * 2 + 1] == 9:
            s += "Programming Aduc failed"
        print(s)


def get_id():
    message = tcp.read_holding_registers(slave_id=254, starting_address=0x0000, quantity=9)
    response = tcp.send_message(message, sock)
    res_data = []
    for b in response:
        res_data.append(b.to_bytes(2, byteorder="big"))
    print("Palette identity response")
    print(res_data)


def get_version():
    message = tcp.read_holding_registers(slave_id=254, starting_address=0x0001, quantity=9)
    response = tcp.send_message(message, sock)
    res_data = []
    for b in response:
        res_data.append(b.to_bytes(2, byteorder="big"))
    print("version")
    ba = bytearray(0)
    for b in res_data:
        ba2 = bytearray(b)
        ba.extend(ba2)
    print(ba.decode().strip('\x00'))


def set_power(power, port):
    address = 0x2100 + port
    value = 0x00
    if power == 1:
        value = 0x01
    message = tcp.write_multiple_registers(slave_id=254, starting_address=address, values=[value])
    response = tcp.send_message(message, sock)
    print("Set Power:" + str(power) + " on port :" + str(port))


def presence(port):
    address = 0x1000 + port
    message = tcp.read_holding_registers(slave_id=254, starting_address=address, quantity=1)
    response = tcp.send_message(message, sock)
    res_data = []
    for reg in response:
        for b in reg.to_bytes(2, byteorder="big"):
            res_data.append(b)

    if res_data[1] == 0x01:
        print("Port: " + str(port) + " present")
    else:
        print("Port: " + str(port) + " not present")
    return res_data


def program(chip, port):
    address = 0x0000
    started = 0
    success = 0
    fail = 0
    ret = False
    if chip == 'main':
        address = 0x2000 + port
        started = 0x01
        success = 0x02
        fail = 0x03
    elif chip == 'app':
        address = 0x2020 + port
        started = 0x04
        success = 0x05
        fail = 0x06
    elif chip == 'aduc':
        address = 0x2040 + port
        started = 0x07
        success = 0x08
        fail = 0x09
    else:
        print("Bad target: " + chip)
    print("Start " + chip + " Programming")
    start = timer()
    message = tcp.read_holding_registers(slave_id=254, starting_address=address, quantity=1)
    response = tcp.send_message(message, sock)
    res_data = []
    for reg in response:
        for b in reg.to_bytes(2, byteorder="big"):
            res_data.append(b)
    if res_data[1] == 0x00:
        print("Programming " + chip + " started")
    elif res_data[1] == 0x01:
        print("Programming " + chip + " started, Powered but prescance on port not checked by client")
    elif res_data[1] == 0x02:
        print("Programming " + chip + " not started, not powered.")
        exit(-1)
    elif res_data[1] == 0x03:
        print("Programming " + chip + " not started, port busy")
        exit(-1)
    programming = True
    while programming:
        # get status
        message = tcp.read_holding_registers(slave_id=254, starting_address=0x6000, quantity=16)
        response = tcp.send_message(message, sock)
        res_data = []
        for reg in response:
            for b in reg.to_bytes(2, byteorder="big"):
                res_data.append(b)
        if res_data[(port - 1) * 2] == 0x03 and res_data[(port - 1) * 2 + 1] == success:
            print("Programming " + chip + " Success Port:" + str(port))
            programming = False
            ret = True
        if res_data[(port - 1) * 2 + 1] == started:
            print("Programming " + chip + " Port: " + str(port))
        if res_data[(port - 1) * 2 + 1] == fail:
            print("Programming " + chip + " Failed Port:" + str(port))
            ret = False
            programming = False
        time.sleep(5.0)
    print(res_data)
    end = timer()
    print(chip + " programming time:", end - start)
    return ret


def read_ram(address, eeprom, ip, port):
    ls = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ls.connect((ip, port))


def print_usage():
    help_text = """mgmt_ctrl_mb.py <command> [command option]   
    help
        Displays this help.
    download <type> <filename>
        Type can be one of: 
            main
            app 
            aduc
        Filename is the complete filename to be downloaded via tftp. Files will be downloaded to volatile RAM-disk.
    program <type> <port>
        Type can be one of: 
            main 
            app
            aduc
        Port is the DUT port number 1 to 16
    set_power <1|0> <port>
        Sets power on the selected port
    presence <port>
        Checks presence on the selected port
    get_id
        Gets the MAC-address of the pallet
    status
        Gets status for all DUTs one 16-bit modbus register or two bytes each. displayed as integers in the cli
        | Byte0                       | Byte1                               | 
        |-----------------------------|-------------------------------------|
        |0x00 not powered or detected | 0x00 not started                    |
        |0x01 powered                 | 0x01 Programming Main MCU started   |
        |0x02 detected                | 0x02 Programming Main MCU succeeded |
        |0x03 powered and detected    | 0x03 Programming Main MCU failed    |
        |                             | 0x04 Programming App MCU started    |
        |                             | 0x05 Programming App MCU succeded   |
        |                             |	0x06 Programming App MCU failed     |
        |                             |	0x07 Programming Aduc started       |
        |                             | 0x08 Programming Aduc succeded      | 
        |                             |	0x09 Programming Aduc failed        |
    
    for read/write RAM and EEPROM from cli use mgmt_ctrl.py
    """
    print(help_text)


def main(*args, **kwargs):
    if len(args) != 0:
        if args[0] == "program":
            if int(args[2]) < 1 or int(args[2]) > 16:
                print("Port needs to be in range 1-16")
                return
            if args[1] == "main" or args[1] == "app" or args[1] == "aduc":
                program(args[1], int(args[2]))
                return
            else:
                print("Program argument needs to be: main,app or aduc")
                return
        if args[0] == "download":
            if args[1] == "main" or args[1] == "app" or args[1] == "aduc":
                download(args[1], args[2])
                return
            else:
                print("Download argument needs to be: main,app or aduc")
                return
        if args[0] == "presence":
            if int(args[1]) < 1 or int(args[1]) > 16:
                print("Port needs to be in range 1-16")
                return
            presence(int(args[1]))
            return
        if args[0] == "set_power":
            if int(args[2]) < 1 or int(args[2]) > 16:
                print("Port needs to be in range 1-16")
                return
            if int(args[1]) < 0 or int(args[1]) > 1:
                print("Power needs to be 0 or 1")
                return
            set_power(int(args[1]), int(args[2]))
            return
        if args[0] == "get_id":
            get_id()
            return
        if args[0] == "get_version":
            get_version()
            return
        if args[0] == "status":
            status()
            return
        if args[0] == "help":
            print_usage()
            return
        else:
            print_usage()
    else:
        print_usage()


if __name__ == "__main__":
    main(*sys.argv[1:])
