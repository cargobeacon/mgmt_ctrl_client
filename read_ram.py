#!/usr/bin/env python
import time
import socket

'''
print("Open pallet socket")
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('192.168.20.10', 520))

print("Read pallet MAC")
sock.send(bytearray(b'\x00\x00\x00\x00\x00\x06\xF7\x03\x00\x00\x00\x09'))
data = sock.recv(32)
print(data)

print("Read meter 1 presence")
sock.send(bytearray(b'\x00\x00\x00\x00\x00\x06\xF7\x03\x10\x01\x00\x01'))
data = sock.recv(32)
print(data)

print("Power-up meter 1")
sock.send(bytearray(b'\x00\x00\x00\x00\x00\x07\xF7\x10\x21\x01\x02\x00\x01'))
data = sock.recv(32)
print(data)

print("Read power status")
sock.send(bytearray(b'\x00\x00\x00\x00\x00\x06\xF7\x03\x60\x00\x00\x10'))
data = sock.recv(32)
print(data)
'''

print("Open meter socket")
m_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#m_sock.connect(('localhost', 501))
m_sock.connect(('192.168.20.10', 501))

#print("Send Read RAM request (address 0x28, size 0x04) with CRC")
#m_sock.send(bytearray(b'\x00\x00\x00\x00\x00\x07\xFE\x44\x00\x28\x04\x06\xE7'))
#data = m_sock.recv(15)
#print(data)

print("Send Read RAM request (address 0x28, size 0x04) without CRC")
m_sock.send(bytearray(b'\x00\x00\x00\x00\x00\x05\xFE\x44\x00\x28\x04'))
data = m_sock.recv(15)
print(data)

#print("Send Read RAM request (address 0x28, size 0x04) without CRC")
#m_sock.send(bytearray(b'\x00\x00\x00\x00\x00\x09\xfe\x43\x00\x00\x04\x68\x05\x02\x00'))
#data = m_sock.recv(15)
#print(data)

m_sock.close()



'''
print("Power-down meter 1")
sock.send(bytearray(b'\x00\x00\x00\x00\x00\x07\xF7\x10\x21\x01\x02\x00\x00'))
data = sock.recv(32)
print(data)

sock.close()
'''
